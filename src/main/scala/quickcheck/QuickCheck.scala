package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  property("min1") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }

  property("1") = forAll { (a: Int, b: Int) =>
    findMin(insert(b, insert(a, empty))) == math.min(a, b)
  }

  property("2") = forAll { a: Int =>
    deleteMin(insert(a, empty)) == empty
  }

  property("3") = forAll { heap: H =>
    def reduceHeap(h: =>H, acc: List[Int]): List[Int] = {
      if (isEmpty(h)) acc
      else reduceHeap(deleteMin(h), findMin(h) :: acc)
    }
    val reduced = reduceHeap(heap, List())
    reduced == reduced.sortWith(_ > _)
  }

  property("4") = forAll { (h1: H, h2: H) =>
    findMin(meld(h1, h2)) == math.min(findMin(h1), findMin(h2))
  }

  property("5") = forAll { h: H =>
    meld(empty, h) == h
  }

  property("6") = forAll { h: H =>
    deleteMin(meld(empty, h)) == deleteMin(h)
  }

  property("7") = forAll { a: Int =>
    findMin(deleteMin(insert(a, empty))) == 0
  }

  property("gen1") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h))== m
  }

  lazy val genHeap: Gen[H] = for {
    a <- arbitrary[Int]
    h <- Gen.frequency((1, empty), (9, genHeap))
    hp <- insert(a, h)
  } yield hp

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

}
